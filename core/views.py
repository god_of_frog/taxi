from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.http import HttpResponseNotFound
from django.shortcuts import render
from rest_framework.exceptions import ValidationError
from rest_framework.response import Response
from rest_framework.views import APIView


def not_found(request):
    return HttpResponseNotFound()


@login_required
def vue(request):
    return render(request, 'index.html')


class AuthenticationView(APIView):
    permission_classes = []

    def get(self, request):
        return Response({
            'authenticated': request.user.is_authenticated,
            'username': request.user.username,
        })

    def post(self, request):
        username = request.data.get('username')
        password = request.data.get('password')
        user = authenticate(username=username, password=password)
        if user:
            login(request, user)
            return self.get(request)

        raise ValidationError({'message': 'Invalid credential.'})

    def delete(self, request):
        logout(request)
        return self.get(request)


class PermissionView(APIView):
    def get(self, request):
        perm = self.request.query_params.get('perm') or ''
        if perm == 'is_staff':
            has = request.user.is_staff
        elif perm:
            has = request.user.has_perm(perm)
        else:
            has = False

        return Response(has)
