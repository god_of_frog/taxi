from django.db.models import NOT_PROVIDED, Field

from utils.format_utils import check_if_constant_name
from utils.object_utils import Dummy

CONSTANT_MODULE_MAPPING = {}


class _ConstantStoreMeta(type):
    def __new__(metacls, cls, bases, classdict):
        classdict['_meta'] = Dummy(fields={})
        for key, value in classdict.items():
            if check_if_constant_name(key):
                assert isinstance(value, Field), 'constant attribute must be instance of django.db.models.Field'
                classdict['_meta'].fields[key] = value

        for field in classdict['_meta'].fields:
            classdict.pop(field)

        newcls = super().__new__(metacls, cls, bases, classdict)
        CONSTANT_MODULE_MAPPING[newcls.__module__.split('.')[-2]] = newcls
        return newcls

    def __getattribute__(cls, key):
        _meta = type.__getattribute__(cls, '_meta')
        if key in _meta.fields:
            field = _meta.fields[key]
            from core.models import Constant
            try:
                return Constant.objects.get(name=cls.get_db_name(key)).value
            except Constant.DoesNotExist:
                return cls.set_constant_value(key, field.default).value
            except:
                return None
        else:
            return type.__getattribute__(cls, key)

    def __setattr__(cls, key, value):
        _meta = type.__getattribute__(cls, '_meta')
        if key in _meta.fields:
            cls.set_constant_value(key, value)
        else:
            type.__setattr__(cls, key, value)

    def __iter__(cls):
        _meta = type.__getattribute__(cls, '_meta')
        return (cls.get_db_name(key) for key in _meta.fields)

    def set_constant_value(cls, key, value):
        from core.models import Constant
        constant, created = Constant.objects.get_or_create(name=cls.get_db_name(key))
        constant.value = None if value is NOT_PROVIDED else value
        constant.save()
        return constant

    def get_db_name(cls, name):
        module = cls.__module__.split('.')[-2]
        return f'{module}.{name}'


class ConstantStore(object, metaclass=_ConstantStoreMeta):
    pass
