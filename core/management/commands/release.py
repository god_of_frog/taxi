import os
import re

from django.core.management import BaseCommand, call_command
from django.utils import timezone

from utils.cmd_utils import run_sync

RELEASE_NOTE_FILE = 'release-note.md'
IGNORED_COMMIT_PATTERN = "(update staticfiles|Merge branch .*|Merge remote-tracking .*)"


def create_release_note(tag, other='origin/built'):
    if not os.path.exists(RELEASE_NOTE_FILE):
        tags = []
        for line in run_sync('git ls-remote --tags origin')[0].splitlines():
            match = re.search(r'refs/tags/(release-\d{4}-\d{2}-\d{2}-\d{2})$', line)
            if match:
                tags.append(match.group(1))

        with open(RELEASE_NOTE_FILE, 'w', encoding='utf-8'):
            pass

        tags = sorted(tags)
        for tag1, tag2 in zip(tags[:-1], tags[1:]):
            create_release_note(tag2, tag1)

    with open(RELEASE_NOTE_FILE, 'r+', encoding='utf-8') as f:
        result = []
        for line in reversed(run_sync(f'git log {tag}...{other} --format="%s"')[0].splitlines()):
            if not re.match(f'^{IGNORED_COMMIT_PATTERN}$', line):
                line = '1. ' + line.replace('\\', '\\\\').replace('_', '\_').replace('*', '\*')
                if line not in result:
                    result.append(line)

        if not result:
            return

        result.insert(0, '# ' + tag)
        content = f.read()
        f.seek(0, 0)
        f.write('\n'.join(result) + '\n\n' + content)

    return True


class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument(
            '--alter', action='store_true', dest='alter',
            help='Wait for you to change something after branch has merged.'
        )
        parser.add_argument(
            '--build', action='store_true', dest='build',
            help='Force build webpack even no change occurred.'
        )

    def handle(self, *args, **kwargs):
        branch = run_sync('git rev-parse --abbrev-ref HEAD')[0].strip()
        assert branch, 'get branch failed'

        if branch == 'master':
            release = 'built'
            tag = 'release'
        else:
            release = f'uat/{branch}'
            tag = f'uat-{branch}'

        assert os.system(f'git push origin {branch}') == 0, 'push current branch failed'
        assert os.system('git fetch origin') == 0, 'fetch failed'

        if os.system(f'git checkout {release}'):
            assert os.system(f'git checkout built') == 0, '"build" branch is required before release'
            assert os.system(f'git checkout -b {release}') == 0, 'new branch failed'
            compare_branch = 'built'
        else:
            assert os.system(f'git reset origin/{release} --hard') == 0, 'reset failed'
            compare_branch = release

        if os.system(f'git merge {branch} --no-edit --no-ff') != 0:
            while True:
                if input('merge failed, fix conflict and type "Y" to retry: ') == 'Y':
                    if os.system('git commit --no-edit') == 0:
                        break
                    else:
                        continue
                else:
                    return

        if kwargs.get('alter'):
            while True:
                if input('alteration start, update your files and commit every change then press Y to continue: ') == 'Y':
                    break

        if os.system(f'git ls-files --error-unmatch static') or os.system(f'git diff {release} origin/{compare_branch} --quiet vue'):
            os.chdir('vue')
            assert os.system('npm run build') == 0, 'build failed'
            os.chdir('..')
            call_command('collectstatic', '-c', '--no-input')
            assert os.system('git add static') == 0, 'add failed'
            os.system('git commit -m "update staticfiles"')

        if os.system(f'git describe --exact-match --tags "{release}"'):
            today = timezone.now().astimezone().strftime(f'{tag}-%Y-%m-%d-XX')
            i = 0
            while True:
                tag = today.replace('XX', str(i).rjust(2, '0'))
                if os.system(f'git tag {tag}') == 0:
                    break
                i += 1

            with open('version', 'w') as f:
                f.write(tag)

            if create_release_note(tag, f'origin/{compare_branch}'):
                assert os.system(f'git add {RELEASE_NOTE_FILE}') == 0, 'add release-note failed'

            assert os.system('git add version') == 0, 'add version failed'
            assert os.system('git commit --amend --no-edit') == 0, 'commit version/release-note failed'
            assert os.system(f'git tag -d {tag}') == 0, 'remove tag failed'
            assert os.system(f'git tag {tag}') == 0, 're tag failed'

        assert os.system(f'git push origin {release}') == 0, 'push failed'
        assert os.system('git push origin --tags') == 0, 'push tags failed'
        assert os.system(f'git checkout {branch}') == 0, 'return failed'
