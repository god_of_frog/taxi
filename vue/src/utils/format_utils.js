import moment from 'moment'

function formatThaiDate(value) {
  let date = moment(value, true)
  if (date.isValid()) {
    return date.format(`DD/MM/${date.year() + 543}`)
  } else {
    return ''
  }
}

function formatThaiDateTime(value) {
  let date = moment(value, true)
  if (date.isValid()) {
    return date.format(`DD/MM/${date.year() + 543} HH:mm`)
  } else {
    return ''
  }
}

export { formatThaiDate, formatThaiDateTime }
