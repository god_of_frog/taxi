/**
 * Define all of your application routes here
 * for more information on routes, see the
 * official documentation https://router.vuejs.org/en/
 */
export default [
{path:'/employee/',view:'Employee'},
{path:'/car/',view:'Car'},
{path:'/home/',view:'Home'},
{path:'/debt/',view:'Debt'},
]
