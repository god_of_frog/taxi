import Vue from 'vue'

import axios from 'axios'
import lodash from 'lodash'

axios.defaults.xsrfCookieName = 'csrftoken'
axios.defaults.xsrfHeaderName = 'X-CSRFToken'

let authenticate
let authenticated = new Promise(resolver => {
  axios.get('/api/core/auth/').then(response => {
    if (response.data.authenticated) {
      resolver(response.data.username)
    }
  })
  authenticate = (username, password) => {
    return axios.post('/api/core/auth/', { username, password }).then(response => {
      if (response.data.authenticated) {
        resolver(response.data.username)
      } else {
        throw 'failed'
      }
    })
  }
})

Vue.prototype.$ajax = {
  authenticated,
  authenticate,
  get() {
    return this.authenticated.then(() => {
      return axios.get.apply(axios, arguments)
    })
  },
  post() {
    return this.authenticated.then(() => {
      return axios.post.apply(axios, arguments)
    })
  },
  put() {
    return this.authenticated.then(() => {
      return axios.put.apply(axios, arguments)
    })
  },
  delete() {
    return this.authenticated.then(() => {
      return axios.delete.apply(axios, arguments)
    })
  },
  upload(url, data) {
    let formData = new FormData()
    lodash.forEach(data, (value, key) => {
      formData.append(key, value)
    })
    return axios.post(url, formData)
  },
}
