import Vue from 'vue'

Vue.directive('perm', {
  /*  Use to show/hide component base on permission name
      Usage (should be written in root node of a component):
        v-query="'bar'"  // show component if user has permission codename of "bar"
   */
  bind(el, binding, vnode) {
    let perm = binding.value
    el.style.display = 'none'
    vnode.context.$root.perm(perm).then(has => {
      if (has) {
        el.style.display = ''
      }
    })
  },
})
