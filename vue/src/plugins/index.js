import './axios'
import './chartist'
import './v-perm'

import Vue from 'vue'
import AsyncComputed from 'vue-async-computed'

Vue.use(AsyncComputed)
