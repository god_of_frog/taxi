from rest_framework import serializers

from apps.taxicom.models import (Car, Employee, Debt, Part, CarUsageLog)


class CarSerializer(serializers.ModelSerializer):
    fuel_type_name = serializers.CharField(source='fuel_type.name', read_only=True)
    plate_province = serializers.CharField(source='plate_province.name', read_only=True)
    car_model = serializers.CharField(source='car_model.name', read_only=True)
    color = serializers.CharField(source='color.name', read_only=True)
    transmission_type = serializers.CharField(source='transmission_type.name', read_only=True)
    last_driver_id = serializers.IntegerField(read_only=True)
    last_time_out = serializers.DateTimeField(read_only=True)
    last_time_in = serializers.DateTimeField(read_only=True)

    class Meta:
        model = Car
        exclude = ['fuel_type']


class TakeCarSerializer(serializers.Serializer):
    car = serializers.PrimaryKeyRelatedField(queryset=Car.objects.filter(status=Car.STATUS.IDLE))
    driver = serializers.PrimaryKeyRelatedField(queryset=Employee.objects.all())
    time_out = serializers.DateTimeField()
    time_in = serializers.DateTimeField()
    price_per_day = serializers.IntegerField()

    def create(self, validated_data):
        validated_data['car'].status = Car.STATUS.OCCUPIED
        validated_data['car'].save()

        CarUsageLog.objects.create(
            car=validated_data['car'],
            driver=validated_data['driver'],
            user=validated_data['user'],
            time_out=validated_data['time_out'],
            time_in=validated_data['time_in'],
            price_per_day=validated_data['price_per_day'],
        )
        return validated_data['car']


class ReturnCarSerializer(serializers.Serializer):
    car = serializers.PrimaryKeyRelatedField(queryset=Car.objects.filter(status=Car.STATUS.OCCUPIED))
    driver = serializers.PrimaryKeyRelatedField(queryset=Employee.objects.all())
    time_in = serializers.DateTimeField()

    def create(self, validated_data):
        log = CarUsageLog.objects.get(
            car=validated_data['car'],
            driver=validated_data['driver'],
            time_in=None,
        )

        validated_data['car'].status = Car.STATUS.OCCUPIED
        validated_data['car'].save()

        log.time_in = validated_data['time_in']
        log.save()
        return validated_data['car']


class EmployeeSerializer(serializers.ModelSerializer):
    debt_sum = serializers.DecimalField(max_digits=10, decimal_places=2, read_only=True)
    keyword = serializers.SerializerMethodField()

    def get_keyword(self, instance):
        return f'{instance.first_name} {instance.last_name}'

    class Meta:
        model = Employee
        fields = "__all__"


class DebtSerializer(serializers.ModelSerializer):
    debtor = serializers.CharField(source='debtor.first_name')

    class Meta:
        model = Debt
        fields = "__all__"


class PartSerializer(serializers.ModelSerializer):
    category = serializers.CharField(source='category.name')
    default_supplier = serializers.CharField(source='default_supplier.name')
    default_storage = serializers.CharField(source='default_storage.code')

    class Meta:
        model = Part
        fields = "__all__"
