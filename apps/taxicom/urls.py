from rest_framework.routers import DefaultRouter

from apps.taxicom.views import CarViewSet, EmployeeViewSet, DebtViewSet, PartViewSet, DrivingViewSet

router = DefaultRouter()
router.register('car', CarViewSet)
router.register('employee', EmployeeViewSet)
router.register('debt', DebtViewSet)
router.register('part', PartViewSet)
router.register('driving', DrivingViewSet)
urlpatterns = router.urls
