from django.apps import AppConfig


class TaxicomConfig(AppConfig):
    name = 'apps.taxicom'
