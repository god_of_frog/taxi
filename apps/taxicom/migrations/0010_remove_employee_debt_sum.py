# Generated by Django 3.1.2 on 2020-11-25 16:21

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('taxicom', '0009_employee_debt_sum'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='employee',
            name='debt_sum',
        ),
    ]
