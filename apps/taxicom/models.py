from django.contrib.auth.models import User
from django.core.validators import MinValueValidator
from django.db import models
from django.db.models import Sum

from core.models import CommonModel, LabeledEnum, StatefulModel, EnumField, TransactionalModel


# ==================================================================== SHARED ATTRIBUTE

class Province(CommonModel):
    name = models.CharField(max_length=255)
    description = models.TextField(blank=True)

    def __str__(self):
        return '{}'.format(self.name)


# ==================================================================== HUMAN RESOURCE ATTRIBUTE

class DriverLicenceType(CommonModel):
    name = models.CharField(max_length=255)
    description = models.TextField(blank=True)

    def __str__(self):
        return '{}'.format(self.name)


class Employee(CommonModel):
    class STATUS(LabeledEnum):
        ACTIVE = ''
        RESIGNED = ''
        BANNED = ''

    first_name = models.CharField(max_length=255)
    last_name = models.CharField(max_length=255)
    birth_date = models.DateField(null=True, blank=True)
    identification_number = models.CharField(max_length=20, blank=True)
    identification_expiry = models.DateField(null=True, blank=True)  # add expiry alert view
    joined_date = models.DateField(null=True, blank=True)
    remark = models.TextField(blank=True)
    status = EnumField(STATUS)
    licence_type = models.ForeignKey(DriverLicenceType, models.PROTECT, null=True, blank=True)
    licence_number = models.CharField(max_length=100, blank=True)
    licence_expiry = models.DateField(null=True, blank=True)  # add expiry alert view
    can_work_as_mechanic = models.BooleanField()
    can_work_as_driver = models.BooleanField()
    can_work_as_teller = models.BooleanField()

    def __str__(self):
        return '{} {}'.format(self.first_name, self.last_name)


# ==================================================================== CAR ATTRIBUTE

class CarModel(CommonModel):
    name = models.CharField(max_length=255)
    description = models.TextField(blank=True)
    year = models.IntegerField(validators=[MinValueValidator(2000)])

    def __str__(self):
        return '{} ({})'.format(self.name, self.year)


class CarColor(CommonModel):
    name = models.CharField(max_length=255)
    description = models.TextField(blank=True)

    def __str__(self):
        return '{}'.format(self.name)


class CarTransmissionType(CommonModel):
    name = models.CharField(max_length=255)
    description = models.TextField(blank=True)

    def __str__(self):
        return '{}'.format(self.name)


class CarFuelType(CommonModel):
    name = models.CharField(max_length=255)
    description = models.TextField(blank=True)

    def __str__(self):
        return '{}'.format(self.name)

#

class Car(CommonModel):
    class STATUS(LabeledEnum):
        IDLE = ''
        OCCUPIED = ''
        SERVICING = ''

    plate_number = models.CharField(max_length=10)
    plate_province = models.ForeignKey(Province, models.PROTECT, related_name='+')
    car_model = models.ForeignKey(CarModel, models.PROTECT, related_name='+')
    color = models.ForeignKey(CarColor, models.PROTECT, related_name='+')
    transmission_type = models.ForeignKey(CarTransmissionType, models.PROTECT, related_name='+')
    fuel_type = models.ForeignKey(CarFuelType, models.PROTECT, related_name='+')
    chassis_number = models.TextField(blank=True)
    engine_number = models.TextField(blank=True)
    gas_tube_number = models.TextField(blank=True)
    manufactured_date = models.DateField(null=True, blank=True)
    purchased_date = models.DateField(null=True, blank=True)
    registration_name = models.ForeignKey(Employee, models.PROTECT, related_name='+', null=True, blank=True)
    registration_date = models.DateField(null=True, blank=True)
    tax_date = models.DateField(null=True, blank=True)
    remark = models.TextField(blank=True)
    status = EnumField(STATUS)
    car_price = models.DecimalField(max_digits=11, decimal_places=2)
    def __str__(self):
        return '{}'.format(self.plate_number)


class CarInsuranceType(CommonModel):
    name = models.CharField(max_length=255)
    description = models.TextField(blank=True)

    def __str__(self):
        return '{}'.format(self.name)


# ==================================================================== PART ATTRIBUTE

class Supplier(CommonModel):
    name = models.CharField(max_length=255)

    def __str__(self):
        return '{}'.format(self.name)


class Storage(CommonModel):
    code = models.CharField(max_length=30)
    description = models.TextField(blank=True)

    def __str__(self):
        return '{}'.format(self.code)


class PartCategory(CommonModel):
    name = models.CharField(max_length=255)

    def __str__(self):
        return '{}'.format(self.name)


# ==================================================================== ACCOUNTING ATTRIBUTE

class Part(CommonModel):
    name = models.CharField(max_length=255)
    category = models.ForeignKey(PartCategory, models.PROTECT, related_name='+')
    description = models.TextField(blank=True)
    part_number = models.TextField(blank=True)
    legacy_code = models.CharField(max_length=20)
    compatibility = models.ManyToManyField(CarModel, related_name='parts')
    default_supplier = models.ForeignKey(Supplier, models.SET_NULL, null=True, blank=True, related_name='+')
    default_storage = models.ForeignKey(Storage, models.SET_NULL, null=True, blank=True, related_name='+')
    minimum_stock = models.PositiveIntegerField()
    alert_stock = models.PositiveIntegerField()
    full_stock = models.PositiveIntegerField()
    batch_size = models.PositiveIntegerField()

    def __str__(self):
        return '{}'.format(self.name)


class DebtType(CommonModel):
    name = models.CharField(max_length=255)
    description = models.TextField(blank=True)

    def __str__(self):
        return '{}'.format(self.name)


# ==================================================================== TRANSACTIONAL

class CarInsurance(CommonModel):
    class STATUS(LabeledEnum):
        PENDING = ''
        INSURED = ''
        EXPIRED = ''

    status = EnumField(STATUS)
    car = models.ForeignKey(Car, models.PROTECT, related_name='insurances')
    insurance_type = models.ForeignKey(CarInsuranceType, models.PROTECT, related_name='+')
    insurance_number = models.CharField(max_length=20)
    insured_date = models.DateField()
    expiry_date = models.DateField()
    remark = models.TextField(blank=True)


class CarPartRequest(StatefulModel):
    car = models.ForeignKey(Car, models.PROTECT, related_name='part_requests')
    part = models.ForeignKey(Part, models.PROTECT, related_name='requests')
    requester = models.ForeignKey(Employee, models.PROTECT, null=True, blank=True, related_name='+')


class CarPartRequestActionLog(CarPartRequest.action_log_class):
    pass


class CarPartOrder(StatefulModel):
    class STATUS(LabeledEnum):
        PENDING = ''
        ORDERED = ''
        COMPLETED = ''
        CANCELED = ''

    class ACTION(LabeledEnum):
        NEW = ''
        EDIT = ''
        CANCEL = ''
        ORDER = ''
        COMPLETE = ''

    TRANSITION = [
        (None, ACTION.NEW, STATUS.PENDING),
        (STATUS.PENDING, ACTION.EDIT, STATUS.PENDING),
        (STATUS.PENDING, ACTION.CANCEL, STATUS.CANCELED),
        (STATUS.PENDING, ACTION.ORDER, STATUS.ORDERED),
        (STATUS.ORDERED, ACTION.COMPLETE, STATUS.COMPLETED),
    ]

    supplier = models.ForeignKey(Supplier, models.PROTECT, related_name='stocks')
    part = models.ForeignKey(Part, models.PROTECT, related_name='stocks')
    amount = models.PositiveIntegerField()
    unit_price = models.DecimalField(max_digits=10, decimal_places=2)
    total_price = models.DecimalField(max_digits=10, decimal_places=2)


class CarPartOrderActionLog(CarPartOrder.action_log_class):
    pass


class PartStock(StatefulModel):
    class STATUS(LabeledEnum):
        ORDERED = ''  # กำลังอยู่ระหว่างการสั่งซื้อ
        STOCK = ''  # เก็บอยู่ในคลังอะไหล่
        USED = ''  # กำลังใช้งาน
        BROKEN = ''  # เสียหาย
        CANCELED = ''  # ยกเลิกคำสั่งซื้อ

    part = models.ForeignKey(Part, models.PROTECT, related_name='+')
    storage = models.ForeignKey(Storage, models.PROTECT, related_name='+')
    order_from = models.ForeignKey(CarPartOrder, models.PROTECT, related_name='+', null=True, blank=True)
    used_in = models.ForeignKey(CarPartRequest, models.PROTECT, related_name='+', null=True, blank=True)


class PartStockActionLog(PartStock.action_log_class):
    pass


class Debt(TransactionalModel):
    value = models.DecimalField(max_digits=11, decimal_places=2)
    debtor = models.ForeignKey(Employee, models.PROTECT, related_name='+')
    is_approved = models.BooleanField(default=False)
    debt_type = models.ForeignKey(DebtType, models.PROTECT, related_name='+')


class CarUsageLog(CommonModel):
    car = models.ForeignKey(Car, models.PROTECT, related_name='+')
    driver = models.ForeignKey(Employee, models.PROTECT, related_name='+')
    time_out = models.DateTimeField(null=True, blank=True)
    time_in = models.DateTimeField(null=True, blank=True)
    user = models.ForeignKey(User, models.PROTECT, related_name='+')
