from django.contrib import admin

from apps.taxicom.models import (
    CarModel, CarColor, CarTransmissionType, CarFuelType, Car, PartCategory, Supplier,
    Storage, Part, Province, DriverLicenceType, Employee, CarPartOrder, Debt, CarUsageLog, DebtType,
)
from core.admin import StatefulModelAdmin, TransactionalModelAdmin

admin.site.register(Province, list_display='__all__')

admin.site.register(CarModel, list_display='__all__')
admin.site.register(CarColor, list_display='__all__')
admin.site.register(CarTransmissionType, list_display='__all__')
admin.site.register(CarFuelType, list_display='__all__')
admin.site.register(Car, list_display='__all__')
admin.site.register(CarUsageLog, list_display='__all__')
admin.site.register(Supplier, list_display='__all__')
admin.site.register(Storage, list_display='__all__')
admin.site.register(PartCategory, list_display='__all__')
admin.site.register(Part, list_display='__all__', list_filter=['category'])


admin.site.register(CarPartOrder, StatefulModelAdmin, list_display='__all__')

admin.site.register(DriverLicenceType, list_display='__all__')
admin.site.register(Employee, list_display='__all__')
admin.site.register(Debt, TransactionalModelAdmin, list_display='__all__')
admin.site.register(DebtType, list_display='__all__')
