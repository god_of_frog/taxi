from django.db.models import OuterRef, Subquery, Sum, Count, Q
from rest_framework import status
from rest_framework.decorators import action, api_view
from rest_framework.parsers import JSONParser
from rest_framework.response import Response
from rest_framework.viewsets import ReadOnlyModelViewSet

from apps.taxicom.models import Car, Employee, Debt, Part, CarUsageLog, DebtType
from apps.taxicom.serializers import CarSerializer, EmployeeSerializer, DebtSerializer, PartSerializer, TakeCarSerializer, ReturnCarSerializer
from utils.format_utils import parse_datetime


class CarViewSet(ReadOnlyModelViewSet):
    queryset = Car.objects.all()
    serializer_class = CarSerializer

    def filter_queryset(self, queryset):
        logs = CarUsageLog.objects.filter(car=OuterRef('id')).order_by('-id')
        queryset = queryset.annotate(
            last_time_out=Subquery(logs.values('time_out')[:1]),
            last_time_in=Subquery(logs.values('time_in')[:1]),
            last_driver_id=Subquery(logs.values('driver_id')[:1]),
        )

        return queryset

    @action(detail=False)
    def get_by_plate_number(self, request):
        queryset = self.filter_queryset(self.get_queryset())
        return Response(CarSerializer(instance=queryset.get(plate_number=request.query_params['plate_number'])).data)

    @action(detail=False)
    def get_status_summary(self, request):
        return Response({
            item['status']: item['count']
            for item in Car.objects.all().values('status').annotate(count=Count('id'))
        })

    @action(detail=False, methods=['POST'])
    def take_car(self, request):
        serializer = TakeCarSerializer(data=request.data)
        serializer.is_valid(True)
        serializer.save(user=request.user)
        return Response()

    @action(detail=False, methods=['POST'])
    def return_car(self, request):
        serializer = ReturnCarSerializer(data=request.data)
        serializer.is_valid(True)
        serializer.save(user=request.user)
        return Response()


class PartViewSet(ReadOnlyModelViewSet):
    queryset = Part.objects.all()
    serializer_class = PartSerializer


class DrivingViewSet(ReadOnlyModelViewSet):
    serializer_class = TakeCarSerializer
    queryset = CarUsageLog.objects.all()

    @action(detail=False)
    def filter_queryset(self, queryset):
        return queryset


class EmployeeViewSet(ReadOnlyModelViewSet):
    serializer_class = EmployeeSerializer
    queryset = Employee.objects.all()

    @action(detail=False)
    def filter_queryset(self, queryset):
        debt_sum = Debt.objects.filter(debtor=OuterRef('pk')).values('debtor').annotate(allsum=Sum('value'))
        queryset = queryset.annotate(debt_sum=Subquery(debt_sum.values('allsum')))

        if self.request.query_params.get('over_500'):
            queryset = queryset.filter(debt_sum__gte=500)

        keyword = self.request.query_params.get('keyword')
        if keyword:
            queryset = queryset.filter(Q(first_name__icontains=keyword) | Q(last_name__icontains=keyword))

        return queryset

    @action(detail=False)
    def sum_debt_employee(self, queryset):
        debt_sum = Debt.objects.filter(debtor=OuterRef('pk')).values('debtor').annotate(allsum=Sum('value'))
        queryset = queryset.annotate(debt_sum=Subquery(debt_sum.values('allsum')))
        return queryset


class DebtViewSet(ReadOnlyModelViewSet):
    serializer_class = DebtSerializer
    queryset = Debt.objects.all()

    @action(detail=False, methods=['POST'])
    def debt_added(self, request):
        debt_type = DebtType.objects.filter(name='ค่าเช่า').last()
        for car in Car.objects.filter(status='OCCUPIED'):
            log = CarUsageLog.objects.filter(car=car).last()
            Debt.objects.create(debtor=log.driver, value=car.car_price, created_by=request.user, updated_by=request.user, debt_type=debt_type)
        return Response()


