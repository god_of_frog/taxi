import subprocess


def run_sync(command, cwd=None, **kwargs):
    p = subprocess.Popen(command.format(**kwargs), stdout=subprocess.PIPE, stderr=subprocess.PIPE, cwd=cwd)
    result, error = p.communicate()
    return result.decode('utf-8'), error.decode('utf-8')
