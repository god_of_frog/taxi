from difflib import SequenceMatcher


class Dummy:
    def __init__(self, **kwargs):
        for key in kwargs:
            setattr(self, key, kwargs[key])


def similar(a, b):
    return SequenceMatcher(None, a, b).ratio()
