import os
from typing import Iterable

from PIL import Image
from pdf2image import convert_from_path


def convert_pdf_to_images(input_file):
    this_path = r'C:\workspace\djaks\utils\poppler'
    this_path_env = ';{};'.format(this_path)
    if this_path_env not in os.environ["PATH"]:
        os.environ["PATH"] += this_path_env
    return convert_from_path(input_file)


def save_images_as_pdf(input_files: Iterable, output_file: str):
    if not output_file.endswith('.pdf'):
        output_file = output_file + '.pdf'

    images = []
    for path in input_files:
        image = path if isinstance(path, Image.Image) else Image.open(path)
        images.append(image.convert('RGB'))

    images[0].save(output_file, save_all=True, append_images=images[1:])
    return output_file
