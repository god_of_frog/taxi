openssl genrsa -out ssl-certificate.key 2048
openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout ssl-certificate.key -out ssl-certificate.crt